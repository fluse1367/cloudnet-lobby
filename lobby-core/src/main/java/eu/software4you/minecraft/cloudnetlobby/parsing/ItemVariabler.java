package eu.software4you.minecraft.cloudnetlobby.parsing;

import eu.software4you.configuration.ConfigurationSection;
import eu.software4you.minecraft.ItemAPI;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class ItemVariabler {
    private final ConfigurationSection section;
    private final Player caller;

    public ItemVariabler(final ConfigurationSection section) {
        this(section, null);
    }

    public ItemVariabler(final ConfigurationSection section, final Player caller) {
        this.section = section;
        this.caller = caller;
    }

    public void iteration(final String sectionId, final int startSlot) throws Exception {
        int slot = startSlot;
        String var = "";
        String cmd = "";
        String arg = "";
        if (sectionId.contains("${") && sectionId.contains("}")) {
            final String expr = sectionId.substring(sectionId.indexOf("${") + 2, sectionId.indexOf("}"));
            var = expr.substring(0, expr.indexOf(";"));
            final String cmdExpr = expr.substring(var.length() + 1);
            cmd = cmdExpr.substring(0, cmdExpr.contains(":") ? cmdExpr.indexOf(":") : cmdExpr.length());
            arg = cmdExpr.substring(cmd.length() + (cmdExpr.contains(":") ? 1 : 0));
        }
        final Variabler variabler = new Variabler(this.section.getConfigurationSection(sectionId), this.caller, var);
        for (final String s : this.list(cmd, arg)) {
            variabler.setSection("item");
            variabler.setVarRepl(s);
            final String materialID = variabler.jsonString("id");
            final int durability = variabler.jsonInt("durability");
            final int amount = variabler.jsonInt("amount");
            final String name = variabler.jsonString("name");
            final List<String> lore = variabler.jsonStringList("lore");
            final ArrayList<ItemAPI.EntchantmentStore> enchantments = new ArrayList<ItemAPI.EntchantmentStore>();
            for (final String ench : variabler.jsonStringList("enchantments")) {
                if (ench != null) {
                    if (ench.equals("")) {
                        continue;
                    }
                    final String enchantmentKey = ench.substring(0, ench.indexOf(":"));
                    final int enchantmentLevel = Integer.valueOf(ench.substring(ench.indexOf(":") + 1));
                    final Enchantment enchantment = ItemAPI.getEnchantmentByKey(enchantmentKey);
                    if (enchantment == null) {
                        throw new Exception("Could not find Enchantment " + enchantmentKey);
                    }
                    enchantments.add(new ItemAPI.EntchantmentStore(enchantment, enchantmentLevel));
                }
            }
            final Material mat = Material.matchMaterial(materialID);
            if (mat == null) {
                throw new Exception("Could not find Material " + materialID);
            }
            ItemStack stack = ItemAPI.genItem(mat, name, lore, durability, amount, true, enchantments);
            if (stack.getType().equals(Material.PLAYER_HEAD) && !lore.isEmpty() && lore.get(0).startsWith("owner:")) {
                final String owner = lore.get(0).substring("owner:".length());
                lore.remove(0);
                stack = ItemAPI.genHead(owner, name, lore, true);
                stack.setAmount(amount);
                for (final ItemAPI.EntchantmentStore store : enchantments) {
                    stack.addUnsafeEnchantment(store.enchantment, store.level);
                }
            }
            variabler.setSection("clickaction");
            final List<String> leftaction = variabler.jsonStringList("left");
            final List<String> rightaction = variabler.jsonStringList("right");
            this.item(slot, stack);
            this.actionLeftClick(slot, leftaction);
            this.actionRightClick(slot, rightaction);
            ++slot;
        }
    }

    private List<String> list(final String id, final String arg) {
        if (!eu.software4you.minecraft.cloudnetlobby.module.List.isRegistered(id)) {
            return new ArrayList<String>(Arrays.asList("$"));
        }
        return eu.software4you.minecraft.cloudnetlobby.module.List.list(this.caller, id, arg);
    }

    public abstract void item(final int p0, final ItemStack p1);

    public abstract void actionLeftClick(final int p0, final List<String> p1);

    public abstract void actionRightClick(final int p0, final List<String> p1);
}
