package eu.software4you.minecraft.cloudnetlobby.module;

import org.bukkit.entity.Player;

public abstract class Placeholder extends Module {
    public Placeholder(final String id) {
        super(id);
    }

    public static String replace(final Player caller, final String id, final String arg) {
        Module.validateRegistered(id, "Tried to call a non-registered placeholder ('" + id + "')");
        final Module module = Placeholder.registeredModules.get(id);
        if (!(module instanceof Placeholder)) {
            throw new UnsupportedOperationException("Tried to call module ('" + id + "') as a placeholder, but it isn't");
        }
        String ret = ((Placeholder) module).replace(caller, arg);
        if (ret == null) {
            ret = "null";
        }
        return ret;
    }

    public static boolean isRegistered(final String id) {
        return Placeholder.registeredModules.containsKey(id) && Placeholder.registeredModules.get(id) instanceof Placeholder;
    }

    protected abstract String replace(final Player p0, final String p1);
}
