package eu.software4you.minecraft.cloudnetlobby.configuration;

import eu.software4you.configuration.file.JsonConfiguration;
import eu.software4you.minecraft.cloudnetlobby.Lobby;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.io.File;
import java.io.IOException;

public class Waypoints {
    private static Lobby lobby;
    private static File jsonFile;
    private static JsonConfiguration json;

    public static void init(final Lobby lobby) {
        Waypoints.lobby = lobby;
        Waypoints.jsonFile = new File(lobby.getDataFolder(), "waypoints.json");
        Waypoints.json = JsonConfiguration.loadConfiguration(Waypoints.jsonFile);
    }

    public static Location getWaypoint(final String name) {
        if (!Waypoints.json.isString(name + ".world")) {
            return null;
        }
        final World w = Bukkit.getWorld(Waypoints.json.getString(name + ".world"));
        if (w == null) {
            return null;
        }
        return new Location(w, Waypoints.json.getDouble(name + ".x"), Waypoints.json.getDouble(name + ".y"), Waypoints.json.getDouble(name + ".z"), (float) Waypoints.json.getDouble(name + ".yaw"), (float) Waypoints.json.getDouble(name + ".pitch"));
    }

    public static boolean setWaypoint(final Location loc, final String name) {
        Waypoints.json.set(name + ".world", loc.getWorld().getName());
        Waypoints.json.set(name + ".x", loc.getX());
        Waypoints.json.set(name + ".y", loc.getY());
        Waypoints.json.set(name + ".z", loc.getZ());
        Waypoints.json.set(name + ".yaw", loc.getYaw());
        Waypoints.json.set(name + ".pitch", loc.getPitch());
        return save();
    }

    public static boolean removeWaypoint(final String name) {
        Waypoints.json.set(name, null);
        return save();
    }

    private static boolean save() {
        try {
            Waypoints.json.save(Waypoints.jsonFile);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
