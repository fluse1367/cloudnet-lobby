package eu.software4you.minecraft.cloudnetlobby.module;

import eu.software4you.configuration.InvalidConfigurationException;
import eu.software4you.configuration.file.YamlConfiguration;
import eu.software4you.minecraft.cloudnetlobby.Lobby;
import eu.software4you.utils.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

public abstract class Module {
    public static final Lobby lobby;
    static final LinkedHashMap<String, Module> registeredModules;

    static {
        lobby = null;
        registeredModules = new LinkedHashMap<String, Module>();
    }

    protected final String id;
    private final File dataFolder;
    private final File configFile;
    private final YamlConfiguration config;
    private final List<String> depends;

    public Module(final String id) {
        this.depends = new ArrayList<String>();
        this.id = id;
        this.dataFolder = new File(Module.lobby.getDataFolder(), "modules/" + this.id);
        this.configFile = new File(this.dataFolder, "config.yml");
        if (this.configFile.exists()) {
            this.config = YamlConfiguration.loadConfiguration(this.configFile);
        } else {
            this.config = new YamlConfiguration();
        }
    }

    protected static void registerModule(final Module module) throws Exception {
        module.register();
    }

    private static void validateNotRegistered(final String id, final String thr) {
        if (isRegistered(id)) {
            throw new IllegalAccessError(thr);
        }
    }

    static void validateRegistered(final String id, final String thr) {
        if (!isRegistered(id)) {
            throw new IllegalAccessError(thr);
        }
    }

    public static boolean isRegistered(final String id) {
        return Module.registeredModules.containsKey(id);
    }

    private void register() throws Exception {
        validateNotRegistered(this.id, "Tried to register an already registered module ('" + this.id + "')");
        Module.registeredModules.put(this.id, this);
        this.load();
    }

    private void unregister() throws Exception {
        validateRegistered(this.id, "Tried to unregister a non-registered module ('" + this.id + ')');
        Module.registeredModules.remove(this.id);
        this.unload();
    }

    public final String getId() {
        return this.id;
    }

    public final File getDataFolder() {
        this.dataFolder.mkdir();
        return this.dataFolder;
    }

    public final File getConfigFile() {
        FileUtils.createNewFile(this.configFile);
        return this.configFile;
    }

    public final YamlConfiguration getConfig() {
        FileUtils.createNewFile(this.configFile);
        return this.config;
    }

    public final void reloadConfig() {
        try {
            this.config.load(this.getConfigFile());
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    public final void saveConfig() {
        try {
            this.config.save(this.configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public final void saveResource(final String resourcePath, final boolean replace) {
        final File f = new File(this.getDataFolder(), resourcePath);
        final File p = new File(f.getParent());
        if (!p.exists()) {
            p.mkdirs();
        }
        FileUtils.saveResource(this.getClass(), resourcePath, p, replace, true);
    }

    public java.util.List<String> getDepends() {
        return Collections.unmodifiableList(this.depends);
    }

    protected abstract void load() throws Exception;

    protected abstract void unload() throws Exception;
}
