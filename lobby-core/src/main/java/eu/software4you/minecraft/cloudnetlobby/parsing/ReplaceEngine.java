package eu.software4you.minecraft.cloudnetlobby.parsing;

import eu.software4you.minecraft.cloudnetlobby.Lobby;
import eu.software4you.minecraft.cloudnetlobby.module.Placeholder;
import eu.software4you.utils.StringUtils;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.entity.Player;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReplaceEngine {
    public static String fullReplace(final Player caller, final String source) {
        return replaceExpressions(caller, placeholderApiReplace(caller, source));
    }

    private static String replaceExpressions(final Player caller, String source) {
        final Pattern pattern = Pattern.compile("\\$\\{[^$\\{}]*\\}");
        while (pattern.matcher(source).find()) {
            final Matcher matcher = pattern.matcher(source);
            while (matcher.find()) {
                final String expression = matcher.group(0);
                source = source.replace(expression, replaceExpression(caller, expression));
            }
        }
        return source.replace("$!§", "$");
    }

    private static String replaceExpression(final Player caller, final String expression) {
        final String expressionBody = replaceExpressions(caller, expression.substring(2, expression.length() - 1));
        final Map.Entry<String, String> e = Lobby.parseModuleCall(expressionBody);
        final String id = e.getKey();
        final String arg = e.getValue();
        String result = "$!§{" + expressionBody + "}";
        if (id.startsWith("?") && StringUtils.containsOneOfArray(id, "=", "<", ">") && arg.startsWith("(") && arg.endsWith(")")) {
            String seperator;
            if (id.contains(">=")) {
                seperator = ">=";
            } else if (id.contains(">")) {
                seperator = ">";
            } else if (id.contains("<")) {
                seperator = "<";
            } else if (id.contains("<=")) {
                seperator = "<=";
            } else if (id.contains("!=")) {
                seperator = "!=";
            } else {
                seperator = "=";
            }
            final String[] strs = id.substring(1).split(seperator);
            final String partA = replaceExpressions(caller, strs[0]);
            final String partB = replaceExpressions(caller, strs[1]);
            boolean expressionResult = false;
            final String s = seperator;
            switch (s) {
                case ">=": {
                    expressionResult = (Integer.parseInt(partA) >= Integer.parseInt(partB));
                    break;
                }
                case ">": {
                    expressionResult = (Integer.parseInt(partA) > Integer.parseInt(partB));
                    break;
                }
                case "<=": {
                    expressionResult = (Integer.parseInt(partA) <= Integer.parseInt(partB));
                    break;
                }
                case "<": {
                    expressionResult = (Integer.parseInt(partA) < Integer.parseInt(partB));
                    break;
                }
                case "=": {
                    expressionResult = partA.equals(partB);
                    break;
                }
                case "!=": {
                    expressionResult = !partA.equals(partB);
                    break;
                }
            }
            if (expressionResult) {
                result = replaceExpressions(caller, arg.substring(1, arg.contains(")else(") ? arg.indexOf(")else(") : (arg.length() - 1)));
            } else {
                result = (arg.contains(")else(") ? replaceExpressions(caller, arg.substring(arg.indexOf(")else(") + 6, arg.length() - 1)) : "");
            }
        } else if (Placeholder.isRegistered(id)) {
            result = Placeholder.replace(caller, id, arg);
        }
        return expression.replace(expression, result);
    }

    private static String placeholderApiReplace(final Player caller, String source) {
        if (caller != null && Lobby.placeholderAPI) {
            source = PlaceholderAPI.setPlaceholders(caller, source);
        }
        return source;
    }
}
