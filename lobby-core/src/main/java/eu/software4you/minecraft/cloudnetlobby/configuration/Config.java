package eu.software4you.minecraft.cloudnetlobby.configuration;

import eu.software4you.configuration.file.YamlConfiguration;
import eu.software4you.minecraft.cloudnetlobby.Lobby;

import java.io.File;
import java.util.List;

public enum Config {
    worldDaytimeChange("world.daytimeChange"),
    worldBlockChange("world.blockChange"),
    worldWeatherChange("world.weatherChange"),
    spawnAnimals("spawn.animals"),
    spawnMonsters("spawn.monsters"),
    spawnNpcs("spawn.npcs"),
    spawnOthers("spawn.others"),
    playerBlockInventory("player.blockInventory"),
    playerDamageTake("player.damage.take"),
    playerDamageRefill("player.damage.refill"),
    playerHungerTake("player.hunger.take"),
    playerHungerRefill("player.hunger.refill"),
    playerPvp("player.pvp"),
    playerPveMake("player.pve.make"),
    playerPveTake("player.pve.take"),
    playerJoinActions("player.joinactions"),
    chatDisableJoinMessage("chat.disable.joinMessage"),
    chatDisableQuitMessage("chat.disable.quitMessage");

    private static Lobby lobby;
    private static File yamlFile;
    private static YamlConfiguration yaml;
    private final String path;

    Config(final String path) {
        this.path = path;
    }

    public static void init(final Lobby lobby) {
        Config.lobby = lobby;
        Config.yamlFile = new File(lobby.getDataFolder(), "config.yml");
        Config.yaml = YamlConfiguration.loadConfiguration(Config.yamlFile);
    }

    @Override
    public String toString() {
        return this.string();
    }

    public String string() {
        return Config.yaml.getString(this.path);
    }

    public int inte() {
        return Config.yaml.getInt(this.path);
    }

    public boolean bool() {
        return Config.yaml.getBoolean(this.path);
    }

    public List<String> list() {
        return Config.yaml.getStringList(this.path);
    }
}
