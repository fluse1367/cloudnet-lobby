package eu.software4you.minecraft.cloudnetlobby.module;

import org.bukkit.entity.Player;

public abstract class List extends Module {
    public List(final String id) {
        super(id);
    }

    public static java.util.List<String> list(final Player caller, final String id, final String arg) {
        Module.validateRegistered(id, "Tried to call a non-registered list ('" + id + "')");
        final Module module = List.registeredModules.get(id);
        if (!(module instanceof List)) {
            throw new UnsupportedOperationException("Tried to call module ('" + id + "') as a list, but it isn't");
        }
        return ((List) module).list(caller, arg);
    }

    public static boolean isRegistered(final String id) {
        return List.registeredModules.containsKey(id) && List.registeredModules.get(id) instanceof List;
    }

    protected abstract java.util.List<String> list(final Player p0, final String p1);
}
