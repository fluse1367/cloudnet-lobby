package eu.software4you.minecraft.cloudnetlobby.listeners;

import eu.software4you.minecraft.cloudnetlobby.Lobby;
import org.bukkit.Bukkit;
import org.bukkit.GameRule;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class WorldListener {
    public static class DaytimeChange {
        public void realTime(final Lobby lobby) {
            this.disable();
            final Calendar calendar = new GregorianCalendar();
            final double hourFactor = 1000.0;
            final double minuteFactor = 16.666666666666668;
            final double secondFactor = 0.2777777777777778;
            final Calendar calendar2 = new GregorianCalendar();
            Bukkit.getScheduler().runTaskTimerAsynchronously(lobby, () -> {
                long ticks = (long) (calendar2.get(Calendar.HOUR_OF_DAY) * hourFactor + calendar2.get(Calendar.MINUTE) * minuteFactor + calendar2.get(Calendar.SECOND) * secondFactor) + 18000L;
                Bukkit.getWorlds().forEach(world -> {
                    world.setFullTime(ticks);
                });
            }, 20L, 20L);
        }

        public void vanilla() {
            Bukkit.getWorlds().forEach(w -> w.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, true));
        }

        public void disable() {
            Bukkit.getWorlds().forEach(w -> w.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false));
        }
    }

    public static class WeatherChange implements Listener {
        public void register(final Lobby lobby) {
            Bukkit.getPluginManager().registerEvents(this, lobby);
        }

        public void unregister() {
            HandlerList.unregisterAll(this);
        }

        @EventHandler
        public void onWeatherChange(final WeatherChangeEvent e) {
            e.setCancelled(true);
        }
    }

    public static class BlockChange implements Listener {
        public void register(final Lobby lobby) {
            Bukkit.getPluginManager().registerEvents(this, lobby);
        }

        public void unregister() {
            HandlerList.unregisterAll(this);
        }

        @EventHandler
        public void handle(final PlayerInteractEvent e) {
            if (e.getAction().equals(Action.PHYSICAL) && e.hasBlock() && e.getClickedBlock().getType().equals(Material.FARMLAND)) {
                e.setCancelled(true);
            }
        }

        @EventHandler
        public void handle(final BlockBreakEvent e) {
            e.setCancelled(true);
        }

        @EventHandler
        public void handle(final BlockPlaceEvent e) {
            e.setCancelled(true);
        }

        @EventHandler
        public void handle(final BlockFromToEvent e) {
            e.setCancelled(true);
        }

        @EventHandler
        public void handle(final BlockDamageEvent e) {
            e.setCancelled(true);
        }

        @EventHandler
        public void handle(final BlockFadeEvent e) {
            e.setCancelled(true);
        }

        @EventHandler
        public void handle(final BlockFertilizeEvent e) {
            e.setCancelled(true);
        }

        @EventHandler
        public void handle(final BlockFormEvent e) {
            e.setCancelled(true);
        }

        @EventHandler
        public void handle(final BlockGrowEvent e) {
            e.setCancelled(true);
        }

        @EventHandler
        public void handle(final BlockIgniteEvent e) {
            e.setCancelled(true);
        }

        @EventHandler
        public void handle(final BlockPistonExtendEvent e) {
            e.setCancelled(true);
        }

        @EventHandler
        public void handle(final BlockPistonRetractEvent e) {
            e.setCancelled(true);
        }

        @EventHandler
        public void handle(final BlockSpreadEvent e) {
            e.setCancelled(true);
        }

        @EventHandler
        public void handle(final CauldronLevelChangeEvent e) {
            e.setCancelled(true);
        }

        @EventHandler
        public void handle(final FluidLevelChangeEvent e) {
            e.setCancelled(true);
        }

        @EventHandler
        public void handle(final LeavesDecayEvent e) {
            e.setCancelled(true);
        }

        @EventHandler
        public void handle(final MoistureChangeEvent e) {
            e.setCancelled(true);
        }

        @EventHandler
        public void handle(final SignChangeEvent e) {
            e.setCancelled(true);
        }

        @EventHandler
        public void handle(final SpongeAbsorbEvent e) {
            e.setCancelled(true);
        }
    }
}
