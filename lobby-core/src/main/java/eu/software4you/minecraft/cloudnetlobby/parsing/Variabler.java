package eu.software4you.minecraft.cloudnetlobby.parsing;

import eu.software4you.configuration.ConfigurationSection;
import eu.software4you.minecraft.cloudnetlobby.Lobby;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Variabler {
    private final ConfigurationSection json;
    private final Player caller;
    private ConfigurationSection section;
    private String var;
    private String varRepl;

    public Variabler(final ConfigurationSection json, final Player caller, final String var) {
        this(json, caller, "", var);
    }

    public Variabler(final ConfigurationSection json, final Player caller, final String section, final String var) {
        this(json, caller, section, var, "");
    }

    public Variabler(final ConfigurationSection json, final Player caller, final String section, final String var, final String varRepl) {
        this.json = json;
        this.caller = caller;
        this.var = var;
        this.varRepl = varRepl;
        this.setSection(section);
    }

    public void setSection(final String path) {
        this.section = this.json.getConfigurationSection(path);
    }

    public void setVar(final String var) {
        this.var = var;
    }

    public void setVarRepl(final String varRepl) {
        this.varRepl = varRepl;
    }

    public int jsonInt(final String key) {
        return Integer.parseInt(this.jsonString(key));
    }

    public String jsonString(final String key) {
        return this.replString(this.section.getString(key));
    }

    public List<String> jsonStringList(final String key) {
        if (this.section.get(key) == null) {
            return Collections.emptyList();
        }
        final List<String> list = new ArrayList<String>();
        for (final Object o : this.section.getList(key, Collections.singletonList(this.section.get(key)))) {
            list.add(this.replString(o.toString()));
        }
        return list;
    }

    private String replString(final Object source) {
        return Lobby.replace(this.caller, String.valueOf(source).replace("$" + this.var, this.varRepl));
    }
}
