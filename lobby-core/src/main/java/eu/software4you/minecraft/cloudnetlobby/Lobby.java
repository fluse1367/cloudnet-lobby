package eu.software4you.minecraft.cloudnetlobby;

import eu.software4you.configuration.file.YamlConfiguration;
import eu.software4you.minecraft.McStringUtils;
import eu.software4you.minecraft.cloudnetlobby.command.CommandManager;
import eu.software4you.minecraft.cloudnetlobby.configuration.Config;
import eu.software4you.minecraft.cloudnetlobby.configuration.Layout;
import eu.software4you.minecraft.cloudnetlobby.configuration.Waypoints;
import eu.software4you.minecraft.cloudnetlobby.listeners.ChatListener;
import eu.software4you.minecraft.cloudnetlobby.listeners.PlayerListener;
import eu.software4you.minecraft.cloudnetlobby.listeners.SpawnListener;
import eu.software4you.minecraft.cloudnetlobby.listeners.WorldListener;
import eu.software4you.minecraft.cloudnetlobby.module.Action;
import eu.software4you.minecraft.cloudnetlobby.module.Module;
import eu.software4you.minecraft.cloudnetlobby.parsing.ReplaceEngine;
import eu.software4you.minecraft.s4yplugin.S4YJavaPlugin;
import eu.software4you.reflection.ReflectUtil;
import eu.software4you.reflection.UniField;
import eu.software4you.utils.FileUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;

import java.io.*;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class Lobby extends S4YJavaPlugin {
    public static boolean placeholderAPI;
    static Lobby instance;
    private static boolean debug;

    static {
        placeholderAPI = false;
        debug = false;
    }

    private Logger logger;

    public static void debug(final String s) {
        if (debug) {
            instance.logger.info("[DEBUG] " + s);
        }
    }

    public static void callActions(final Player caller, final List<String> actions) throws Exception {
        for (final String action : actions) {
            callAction(caller, action);
        }
    }

    public static void callAction(final Player caller, final String line) throws Exception {
        final Map.Entry<String, String> e = parseModuleCall(line);
        Action.call(caller, e.getKey(), e.getValue());
    }

    public static Map.Entry<String, String> parseModuleCall(final String line) {
        final String id = line.substring(0, line.contains(":") ? line.indexOf(":") : line.length());
        final String arg = line.substring(id.length() + (line.contains(":") ? 1 : 0));
        return new AbstractMap.SimpleEntry<String, String>(id, arg);
    }

    public static String replace(final Player caller, final String source) {
        return ReplaceEngine.fullReplace(caller, source);
    }

    public static String replaceC(final Player caller, final String source) {
        return McStringUtils.colorText(replace(caller, source));
    }

    public void onEnable() {
        instance = this;
        this.logger = this.getLogger();
        debug = new File(this.getDataFolder(), "debug").exists();
    }

    public void enable() throws Exception {
        if (!this.init()) {
            Bukkit.getPluginManager().disablePlugin(this);
        }
    }

    public void disable() throws Exception {
        boolean b = false;

        final LinkedHashMap<String, Module> registeredModules = (LinkedHashMap<String, Module>) ReflectUtil.forceCall(Module.class, null, "registeredModules");

        for (final Module module : new ArrayList<>(registeredModules.values())) {
            try {
                this.logger.info("Unregistering module " + module.getId());
                ReflectUtil.forceCall(Module.class, module, "unregister()");
                this.logger.info("Module " + module.getId() + " unregistered!");
            } catch (Exception e) {
                this.getLogger().warning("Error occurred while unregistering module " + module.getId() + ":\n" + ExceptionUtils.getFullStackTrace(e.getCause()));
                b = true;
            }
        }
        registeredModules.clear();
        if (b) {
            Bukkit.broadcastMessage("§b§lCloudNet§6§lLobby §8»§c Error occurred while disabling. See console for details.");
        }
    }

    public String getDefaultLicenseKey() {
        return "WE8N-R1HG-QCII-LIKH";
    }

    private boolean init() {
        try {
            this.getLogger().info("Initiating ...");
            this.saveResources();
            Config.init(this);
            Layout.init(this);
            Waypoints.init(this);
            this.loadModules();
            this.loadListeners();
            new CommandManager(this).registerClass(LobbyCommandHandler.class);
            this.getLogger().info("Initiated!");
            placeholderAPI = (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null);
            if (placeholderAPI) {
                this.getLogger().info("PlaceholderAPI found!");
            }
            Bukkit.getOnlinePlayers().forEach(pp -> Bukkit.getPluginManager().callEvent(new PlayerJoinEvent(pp, (String) null)));
        } catch (Exception e) {
            Bukkit.broadcastMessage("§b§lCloudNet§6§lLobby §8»§c Error occurred while initiating. See console for details.");
            this.getLogger().warning("Error occurred while initiating:");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private void saveResources() throws IOException {
        this.extract("", "config.yml");
        this.extract("", "layout.yml");
        this.extract("", "waypoints.json");
        final File modules = new File(this.getDataFolder(), "modules");
        modules.mkdir();
        final BufferedReader r = new BufferedReader(new InputStreamReader(Lobby.class.getClassLoader().getResourceAsStream("include.txt")));
        String simpleModuleName;
        while ((simpleModuleName = r.readLine()) != null) {
            FileUtils.saveResource(Lobby.class, simpleModuleName + ".jar", modules.getPath(), true, false);
        }
    }

    private void extract(final String dir, final String file) {
        this.extract(dir, file, false);
    }

    private void extract(final String dir, final String file, final boolean replace) {
        FileUtils.saveResource(this.getClass(), "res/" + dir + (dir.equals("") ? "" : "/") + file, new File(this.getDataFolder(), dir), replace, false);
    }

    private void loadListeners() {
        this.loadWorldlisteners();
        this.loadSpawnListeners();
        this.loadPlayerListeners();
        this.loadChatListeners();
    }

    private void loadWorldlisteners() {
        final WorldListener.DaytimeChange dtc = new WorldListener.DaytimeChange();
        if (Config.worldDaytimeChange.string().equals("real")) {
            dtc.realTime(this);
        } else if (Config.worldDaytimeChange.string().equals("vanilla")) {
            dtc.vanilla();
        } else if (Config.worldDaytimeChange.string().equals("false")) {
            dtc.disable();
        }
        if (!Config.worldBlockChange.bool()) {
            new WorldListener.BlockChange().register(this);
        }
        if (!Config.worldWeatherChange.bool()) {
            new WorldListener.WeatherChange().register(this);
        }
    }

    private void loadSpawnListeners() {
        if (!Config.spawnAnimals.bool()) {
            new SpawnListener.Animals().register(this);
        }
        if (!Config.spawnMonsters.bool()) {
            new SpawnListener.Monsters().register(this);
        }
        if (!Config.spawnNpcs.bool()) {
            new SpawnListener.Npcs().register(this);
        }
        if (!Config.spawnOthers.bool()) {
            new SpawnListener.Others().register(this);
        }
    }

    private void loadPlayerListeners() {
        if (Config.playerBlockInventory.bool()) {
            new PlayerListener.InventoryBlock().register(this);
        }
        if (!Config.playerDamageTake.bool()) {
            new PlayerListener.DamageTake().register(this);
        }
        if (Config.playerDamageRefill.bool()) {
            new PlayerListener.DamageRefill().register(this);
        }
        if (!Config.playerHungerTake.bool()) {
            new PlayerListener.HungerTake().register(this);
        }
        if (Config.playerHungerRefill.bool()) {
            new PlayerListener.HungerRefill().register(this);
        }
        if (!Config.playerPvp.bool()) {
            new PlayerListener.Pvp().register(this);
        }
        if (!Config.playerPveMake.bool()) {
            new PlayerListener.PveMake().register(this);
        }
        if (!Config.playerPveTake.bool()) {
            new PlayerListener.PveTake().register(this);
        }
        new PlayerListener.JoinActions().register(this);
    }

    private void loadChatListeners() {
        if (Config.chatDisableJoinMessage.bool()) {
            new ChatListener.DisableJoinMessage().register(this);
        }
        if (Config.chatDisableQuitMessage.bool()) {
            new ChatListener.DisableQuitMessage().register(this);
        }
    }

    private void loadModules() throws Exception {
        ReflectUtil.forceCallGuess(Module.class, null, "lobby", this);
        final List<Module> loadedModules = new ArrayList<Module>();
        for (final File file : new File(this.getDataFolder(), "modules").listFiles(f -> f.getName().toLowerCase().endsWith(".jar"))) {
            try {
                this.logger.info("Loading module " + file.getName());
                final Module module = (Module) this.loadModule(file);
                loadedModules.add(module);
                this.logger.info("Module " + module.getId() + " ('" + file.getName() + "') loaded!");
            } catch (Exception e) {
                this.getLogger().warning("Error while loading module " + file.getName() + ":\n" + ExceptionUtils.getFullStackTrace(e));
            }
        }
        for (final Module module : loadedModules) {
            try {
                this.logger.info("Registering module " + module.getId());
                ReflectUtil.forceCallGuess(Module.class, module, "register");
                this.logger.info("Module " + module.getId() + " registered!");
            } catch (Exception e2) {
                this.getLogger().warning("Error occurred while registering module " + module.getId() + ":\n" + ExceptionUtils.getFullStackTrace(e2.getCause()));
            }
        }
    }

    private Object loadModule(final File jarFile) throws Exception {
        final ZipFile zip = new ZipFile(jarFile);
        final ZipEntry entry = zip.getEntry("module.yml");
        if (entry == null) {
            throw new Exception("Module file must have an module description file ('module.yml')");
        }
        final InputStream in = zip.getInputStream(entry);
        final YamlConfiguration c = new YamlConfiguration();
        c.load(new InputStreamReader(in));
        in.close();
        final String main = c.getString("main");
        if (main == null) {
            throw new Exception("Module main path cannot be null");
        }
        final List<String> depends = c.isList("depends") ? c.getStringList("depends") : new ArrayList<String>();
        for (final String depend : depends) {
            if (Bukkit.getPluginManager().getPlugin(depend) == null) {
                throw new Exception(String.format("Module dependency %s not found", depend));
            }
        }
        final ClassLoader cl = new URLClassLoader(new URL[]{jarFile.toURI().toURL()}, this.getClass().getClassLoader());
        Class clazz = null;
        try {
            clazz = Class.forName(main, true, cl);
        } catch (ClassNotFoundException e) {
            throw new Exception("Cannot find main class " + main);
        }
        Constructor constructor;
        try {
            constructor = clazz.getDeclaredConstructor();
        } catch (NoSuchMethodException e2) {
            throw new Exception("Constructor of " + main + " must have no parameters");
        }
        constructor.setAccessible(true);
        final Object instance = constructor.newInstance();
        if (!(instance instanceof Module)) {
            throw new Exception("Main class must be an instance of eu.software4you.minecraft.cloudnetlobby.modules.Module");
        }
        ((List) new UniField(Module.class.getDeclaredField("depends")).get(instance)).addAll(depends);
        return instance;
    }

    public String id() {
        return "cnLobby";
    }

    public String suffix() {
        return "cnlobby";
    }
}
