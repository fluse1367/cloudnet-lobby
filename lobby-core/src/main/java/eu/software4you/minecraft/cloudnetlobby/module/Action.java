package eu.software4you.minecraft.cloudnetlobby.module;

import org.bukkit.entity.Player;

public abstract class Action extends Module {
    public Action(final String id) {
        super(id);
    }

    public static void call(final Player caller, String id, final String arg) throws Exception {
        final boolean quit = id.startsWith("!");
        if (quit) {
            id = id.substring(1);
        }
        if (id.equals("")) {
            return;
        }
        Module.validateRegistered(id, "Tried to call a non-registered action ('" + id + "')");
        final Module module = Action.registeredModules.get(id);
        if (!(module instanceof Action)) {
            throw new UnsupportedOperationException("Tried to call module ('" + id + "') as an action, but it isn't");
        }
        ((Action) module).call(caller, arg, quit);
    }

    public static boolean isRegistered(final String id) {
        return Action.registeredModules.containsKey(id) && Action.registeredModules.get(id) instanceof Action;
    }

    protected abstract boolean call(Player caller, String arg, final boolean quiet) throws Exception;
}
