package eu.software4you.minecraft.cloudnetlobby.module;

import de.dytanic.cloudnet.driver.service.ServiceInfoSnapshot;
import de.dytanic.cloudnet.wrapper.Wrapper;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicInteger;

public class ServerGroups extends List {
    public ServerGroups() {
        super("ServerGroups");
    }

    protected java.util.List<String> list(final Player caller, final String arg) {
        final java.util.List<String> li = new ArrayList<String>();
        final AtomicInteger online = new AtomicInteger(0);
        final Collection<ServiceInfoSnapshot> services = Wrapper.getInstance().getCloudService(arg);
        if (services == null) {
            return Collections.emptyList();
        }
        services.forEach(serviceInfoSnapshot -> {
            if (serviceInfoSnapshot.isConnected()) {
                online.incrementAndGet();
            }
            return;
        });
        for (int i = 0; i < online.get(); ++i) {
            li.add(arg + "-" + (i + 1));
        }
        return li;
    }

    protected void load() throws Exception {
    }

    protected void unload() throws Exception {
    }
}
