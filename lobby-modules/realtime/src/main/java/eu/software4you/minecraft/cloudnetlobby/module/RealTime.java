package eu.software4you.minecraft.cloudnetlobby.module;

import org.bukkit.entity.Player;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RealTime extends Placeholder {
    public RealTime() {
        super("RealTime");
    }

    protected String replace(final Player caller, final String arg) {
        return new SimpleDateFormat(arg).format(new Date());
    }

    protected void load() throws Exception {
    }

    protected void unload() throws Exception {
    }
}
