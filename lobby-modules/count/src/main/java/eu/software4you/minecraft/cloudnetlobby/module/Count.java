package eu.software4you.minecraft.cloudnetlobby.module;

import org.bukkit.entity.Player;

import java.util.ArrayList;

public class Count extends List {
    public Count() {
        super("Count");
    }

    protected java.util.List<String> list(final Player caller, final String arg) {
        final java.util.List<String> li = new ArrayList<String>();
        if (arg.contains(",")) {
            int a = Integer.valueOf(arg.split(",")[0]);
            final int b = Integer.valueOf(arg.split(",")[1]);
            final int i = a;
            while (a <= b) {
                li.add(String.valueOf(i));
                ++a;
            }
        }
        return li;
    }

    protected void load() throws Exception {
    }

    protected void unload() throws Exception {
    }
}
