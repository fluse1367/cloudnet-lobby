package eu.software4you.minecraft.cloudnetlobby.module;

import eu.software4you.configuration.ConfigurationSection;
import eu.software4you.configuration.file.YamlConfiguration;
import eu.software4you.minecraft.EntityUtils;
import eu.software4you.minecraft.McStringUtils;
import eu.software4you.minecraft.ScoreboardManager;
import eu.software4you.minecraft.cloudnetlobby.Lobby;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scoreboard.DisplaySlot;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Scoreboard extends Action {
    private final HashMap<String, YamlConfiguration> registeredScoreboards;
    private final HashMap<Player, Handler> handlers;
    private File scoreboardsDir;
    private boolean alternativeMode;

    public Scoreboard() {
        super("Scoreboard");
        this.registeredScoreboards = new HashMap<String, YamlConfiguration>();
        this.handlers = new HashMap<Player, Handler>();
    }

    protected boolean call(final Player caller, final String arg, final boolean quit) throws Exception {
        if (arg.equals("~done") && this.handlers.containsKey(caller)) {
            this.handlers.get(caller).done();
        } else if (arg.equals("~reload") && this.handlers.containsKey(caller)) {
            final String id = this.handlers.get(caller).id;
            this.handlers.get(caller).done();
            this.call(caller, id, quit);
        } else {
            new Handler(caller, arg).begin();
        }
        return true;
    }

    protected void load() {
        this.scoreboardsDir = new File(this.getDataFolder(), "scoreboards");
        this.saveResource("config.yml", false);
        this.reloadConfig();
        this.alternativeMode = this.getConfig().getBoolean("alternative-mode");
        this.scoreboardsDir.mkdirs();
        this.saveResource("scoreboards/default.yml", false);
        for (final File file : new File(this.getDataFolder(), "scoreboards").listFiles(f -> f.getName().endsWith(".yml"))) {
            final YamlConfiguration yaml = YamlConfiguration.loadConfiguration(file);
            this.registerScoreboard(file.getName().substring(0, file.getName().lastIndexOf(".")), yaml);
        }
    }

    protected void unload() {
        this.handlers.forEach((c, h) -> h.done(false));
        this.handlers.clear();
        this.registeredScoreboards.clear();
    }

    private void validateScoreboardRegistered(final String id, final String thr) {
        if (!this.isScoreboard(id)) {
            throw new IllegalAccessError(thr);
        }
    }

    private void validateScoreboardNotRegistered(final String id, final String thr) {
        if (this.isScoreboard(id)) {
            throw new IllegalAccessError(thr);
        }
    }

    public boolean isScoreboard(final String id) {
        return this.registeredScoreboards.containsKey(id);
    }

    private void registerScoreboard(final String id, final YamlConfiguration yaml) {
        this.validateScoreboardNotRegistered(id, "Tried to register an already registered scoreboard ('" + id + "')");
        this.registeredScoreboards.put(id, yaml);
    }

    private class Handler implements Listener {
        private final YamlConfiguration yaml;
        private final Player caller;
        private final String id;
        private final ScoreboardManager man;
        private String title;
        private double updateInterval;
        private HashMap<SidebarElement, Integer> scores;
        private HashMap<String, Integer> simpleScores;
        private Thread ticker;
        private boolean done;

        private Handler(final Player caller, final String id) throws Exception {
            this.title = "";
            this.updateInterval = -1.0;
            this.scores = new HashMap<SidebarElement, Integer>();
            this.simpleScores = new HashMap<String, Integer>();
            this.ticker = null;
            this.done = false;
            if (!Scoreboard.this.isScoreboard(id)) {
                throw new IllegalAccessError("Tried to load an non-registered scoreboard ('" + id + "') for " + caller.getName());
            }
            if (Scoreboard.this.handlers.containsKey(caller)) {
                throw new IllegalAccessError("Tried to overload scoreboard ('" + Scoreboard.this.handlers.get(caller).id + "') with scoreboard ('" + id + "') for " + caller.getName());
            }
            this.yaml = Scoreboard.this.registeredScoreboards.get(id);
            this.caller = caller;
            this.id = id;
            this.man = ScoreboardManager.getInstance("scoreboard_" + id);
            Scoreboard.this.handlers.put(caller, this);
        }

        private void begin() throws Exception {
            Bukkit.getPluginManager().registerEvents(this, Module.lobby);
            if (this.caller.getScoreboard() != null && this.caller.getScoreboard() != Bukkit.getScoreboardManager().getMainScoreboard()) {
                EntityUtils.getInstance().setMetadata(this.caller, "scoreboard_" + this.id, this.caller.getScoreboard());
            }
            this.parse();
            this.reloadScoreboard();
        }

        private void done() {
            this.done(true);
        }

        private void done(final boolean remove) {
            if (this.done) {
                return;
            }
            this.done = true;
            HandlerList.unregisterAll(this);
            this.ticker.interrupt();
            this.man.getScoreboard(this.caller).getObjective(DisplaySlot.SIDEBAR).unregister();
            this.scores.clear();
            this.simpleScores.clear();
            if (remove) {
                Scoreboard.this.handlers.remove(this.caller, this);
            }
        }

        private void parse() {
            this.title = this.yaml.getString("title");
            this.updateInterval = this.yaml.getLong("updateInterval");
            final ConfigurationSection content = this.yaml.getConfigurationSection("content");
            for (final String num : content.getKeys(false)) {
                SidebarElement elem;
                if (Scoreboard.this.alternativeMode) {
                    if (!content.isConfigurationSection(num) && content.isString(num)) {
                        final String line = content.getString(num);
                        String prefix;
                        String suffix;
                        String body;
                        if (line.length() <= 16) {
                            suffix = (prefix = "");
                            body = line;
                        } else if (line.length() > 32) {
                            prefix = line.substring(0, 16);
                            body = line.substring(16, 32);
                            suffix = line.substring(32);
                        } else {
                            prefix = "";
                            body = line.substring(0, 16);
                            suffix = line.substring(16);
                        }
                        content.set(String.format("%s.prefix", num), prefix);
                        content.set(String.format("%s.body", num), body);
                        content.set(String.format("%s.suffix", num), suffix);
                    }
                    final ConfigurationSection sec = content.getConfigurationSection(num);
                    String body2 = sec.getString("body");
                    if (body2 == null) {
                        continue;
                    }
                    if (body2.isEmpty()) {
                        continue;
                    }
                    if (body2.length() > 16) {
                        body2 = body2.substring(0, 16);
                    }
                    elem = new SidebarElement(sec.getString("prefix"), body2, sec.getString("suffix"));
                } else {
                    if (content.isConfigurationSection(num)) {
                        final ConfigurationSection sec = content.getConfigurationSection(num);
                        content.set(num, sec.getString("prefix") + sec.getString("body") + sec.getString("suffix"));
                    }
                    final String line = content.getString(num);
                    if (line == null) {
                        continue;
                    }
                    if (line.isEmpty()) {
                        continue;
                    }
                    final String body2 = McStringUtils.randomMcColorString(4) + "§r";
                    elem = new SidebarElement("", body2, line);
                }
                final int score = Integer.parseInt(num);
                this.scores.put(elem, score);
                this.simpleScores.put(elem.getBody(), score);
            }
            try {
                this.yaml.save(new File(Scoreboard.this.scoreboardsDir, String.format("%s.yml", this.id)));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void reloadScoreboard() {
            this.man.setSidebar(this.caller, Lobby.replaceC(this.caller, this.title), this.simpleScores);
            if (this.ticker != null) {
                this.ticker.interrupt();
            }
            (this.ticker = new Thread() {
                @Override
                public void run() {
                    while (!this.isInterrupted()) {
                        if (Handler.this.caller == null) {
                            this.interrupt();
                            break;
                        }
                        try {
                            Lobby.debug("Scoreboard Title: \"" + Lobby.replaceC(Handler.this.caller, Handler.this.title) + "\"");
                            Handler.this.caller.getScoreboard().getObjective(DisplaySlot.SIDEBAR).setDisplayName(Lobby.replaceC(Handler.this.caller, Handler.this.title));
                            for (final SidebarElement element : Handler.this.scores.keySet()) {
                                String prefix = Lobby.replaceC(Handler.this.caller, element.getPrefix());
                                final String body = element.getBody();
                                String suffix = Lobby.replaceC(Handler.this.caller, element.getSuffix());
                                if (Scoreboard.this.alternativeMode) {
                                    if (prefix.length() > 16) {
                                        prefix = prefix.substring(0, 16);
                                    }
                                    if (suffix.length() > 16) {
                                        suffix = suffix.substring(0, 16);
                                    }
                                } else if (suffix.length() > 16) {
                                    prefix = suffix.substring(0, 16);
                                    if (prefix.endsWith("§")) {
                                        prefix = prefix.substring(0, prefix.length() - 1);
                                    }
                                    suffix = suffix.substring(prefix.length());
                                    boolean abort = false;
                                    if (suffix.startsWith("§")) {
                                        final Matcher matcher = Pattern.compile("§([0-9a-fr])").matcher(suffix);
                                        if (matcher.find() && matcher.start() == 0) {
                                            abort = true;
                                        }
                                    }
                                    if (!abort) {
                                        String lastColor = "";
                                        int lastColorIndex = -1;
                                        Matcher matcher2 = Pattern.compile("§([0-9a-f])").matcher(prefix);
                                        while (matcher2.find()) {
                                            lastColor = matcher2.group(0);
                                            lastColorIndex = matcher2.start();
                                        }
                                        final StringBuilder lastFormats = new StringBuilder();
                                        matcher2 = Pattern.compile("§([k-or])").matcher(prefix);
                                        while (matcher2.find()) {
                                            final int index = matcher2.start();
                                            if (index < lastColorIndex) {
                                                continue;
                                            }
                                            final String code = matcher2.group(1);
                                            if (code.equals("r")) {
                                                lastColor = "";
                                                lastColorIndex = -1;
                                                lastFormats.setLength(0);
                                            } else {
                                                if (lastFormats.toString().contains("§" + code)) {
                                                    lastFormats.delete(lastFormats.indexOf("§" + code), lastFormats.indexOf("§" + code) + 2);
                                                }
                                                lastFormats.append("§").append(code);
                                            }
                                        }
                                        suffix = lastColor + lastFormats + suffix;
                                        if (suffix.length() > 16) {
                                            suffix = suffix.substring(0, 16);
                                        }
                                    }
                                }
                                Handler.this.man.updateSidebarEntry(Handler.this.caller, body, body, prefix, suffix);
                            }
                            if (Handler.this.updateInterval > 0.0) {
                                Thread.sleep((long) (Handler.this.updateInterval * 50.0));
                            } else {
                                this.interrupt();
                            }
                        } catch (InterruptedException e) {
                            this.interrupt();
                        }
                    }
                }
            }).start();
        }

        @EventHandler
        public void onQuit(final PlayerQuitEvent e) {
            if (e.getPlayer() == this.caller) {
                this.done();
            }
        }
    }
}
