package eu.software4you.minecraft.cloudnetlobby.module;

public class SidebarElement {
    private final String prefix;
    private final String body;
    private final String suffix;

    public SidebarElement(final String prefix, final String body, final String suffix) {
        this.prefix = prefix;
        this.body = body;
        this.suffix = suffix;
    }

    public String getPrefix() {
        return this.prefix;
    }

    public String getBody() {
        return this.body;
    }

    public String getSuffix() {
        return this.suffix;
    }
}
