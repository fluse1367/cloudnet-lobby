package eu.software4you.minecraft.cloudnetlobby.module;

import eu.software4you.minecraft.cloudnetlobby.configuration.Layout;
import eu.software4you.minecraft.cloudnetlobby.configuration.Waypoints;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class Waypoint extends Action {
    public Waypoint() throws Exception {
        super("Waypoint");
    }

    protected boolean call(final Player caller, final String arg, final boolean quit) {
        final Location wp = Waypoints.getWaypoint(arg);
        if (wp == null) {
            Layout.waypointNotExist.send(caller);
            return false;
        }
        caller.teleport(wp);
        if (quit) {
            return true;
        }
        if (arg.equals("spawn")) {
            Layout.teleportSpawn.send(caller, arg);
        } else {
            Layout.teleportOther.send(caller, arg);
        }
        return true;
    }

    protected void load() {
    }

    protected void unload() {
    }
}
