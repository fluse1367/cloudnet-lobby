package eu.software4you.minecraft.cloudnetlobby.module;

import eu.software4you.minecraft.cloudnetlobby.Lobby;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Broadcast extends Action {
    public Broadcast() throws Exception {
        super("Broadcast");
    }

    protected boolean call(final Player caller, final String arg, final boolean quit) {
        if (arg == null || arg.isEmpty()) {
            return false;
        }
        Bukkit.broadcastMessage(Lobby.replaceC(caller, arg.replace("{newline}", "\n")));
        return true;
    }

    protected void load() {
    }

    protected void unload() {
    }
}
