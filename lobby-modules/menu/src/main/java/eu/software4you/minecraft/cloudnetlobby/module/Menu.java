package eu.software4you.minecraft.cloudnetlobby.module;

import eu.software4you.configuration.ConfigurationSection;
import eu.software4you.configuration.file.JsonConfiguration;
import eu.software4you.minecraft.McStringUtils;
import eu.software4you.minecraft.cloudnetlobby.Lobby;
import eu.software4you.minecraft.cloudnetlobby.parsing.ItemVariabler;
import eu.software4you.minecraft.cloudnetlobby.parsing.Variabler;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.HashMap;
import java.util.List;

public class Menu extends Action {
    private final HashMap<String, JsonConfiguration> registeredMenus;
    private final HashMap<Player, Handler> handlers;

    public Menu() {
        super("Menu");
        this.registeredMenus = new HashMap<String, JsonConfiguration>();
        this.handlers = new HashMap<Player, Handler>();
    }

    protected boolean call(final Player caller, final String arg, final boolean quit) throws Exception {
        if (arg.equals("~done") && this.handlers.containsKey(caller)) {
            this.handlers.get(caller).done();
        } else if (arg.equals("~reload") && this.handlers.containsKey(caller)) {
            final String id = this.handlers.get(caller).id;
            this.handlers.get(caller).done();
            this.call(caller, id, quit);
        } else {
            new Handler(caller, arg).begin();
        }
        return true;
    }

    protected void load() {
        new File(this.getDataFolder(), "menus").mkdirs();
        this.saveResource("menus/lobbies.json", false);
        this.saveResource("menus/navigation.json", false);
        for (final File menu : new File(this.getDataFolder(), "menus").listFiles(f -> f.getName().endsWith(".json"))) {
            final JsonConfiguration json = JsonConfiguration.loadConfiguration(menu);
            this.registerMenu(menu.getName().substring(0, menu.getName().lastIndexOf(".")), json);
        }
    }

    protected void unload() {
        this.registeredMenus.clear();
        this.handlers.forEach((c, h) -> {
            c.closeInventory();
            h.done(false);
            return;
        });
        this.handlers.clear();
    }

    private void validateMenuRegistered(final String id, final String thr) {
        if (!this.isMenu(id)) {
            throw new IllegalAccessError(thr);
        }
    }

    private void validateMenuNotRegistered(final String id, final String thr) {
        if (this.isMenu(id)) {
            throw new IllegalAccessError(thr);
        }
    }

    public boolean isMenu(final String id) {
        return this.registeredMenus.containsKey(id);
    }

    private void registerMenu(final String id, final JsonConfiguration json) {
        this.validateMenuNotRegistered(id, "Tried to register an already registered menu ('" + id + "')");
        this.registeredMenus.put(id, json);
    }

    private void unregisterMenu(final String id) {
        this.validateMenuRegistered(id, "Tried to unregister a non-registered menu ('" + id + ')');
        this.registeredMenus.remove(id);
    }

    private class Handler implements Listener {
        private final JsonConfiguration json;
        private final Player caller;
        private final String id;
        private final HashMap<Integer, List<String>> actionsLeftClick;
        private final HashMap<Integer, List<String>> actionsRightClick;
        private final Inventory inventory;
        private boolean done;

        private Handler(final Player caller, final String id) throws Exception {
            this.actionsLeftClick = new HashMap<Integer, List<String>>();
            this.actionsRightClick = new HashMap<Integer, List<String>>();
            this.done = false;
            if (!Menu.this.isMenu(id)) {
                throw new IllegalAccessError("Tried to load an non-registered menu ('" + id + "') for " + caller.getName());
            }
            if (Menu.this.handlers.containsKey(caller)) {
                throw new IllegalAccessError("Tried to overload menu ('" + Menu.this.handlers.get(caller).id + "') with menu ('" + id + "') for " + caller.getName());
            }
            this.json = Menu.this.registeredMenus.get(id);
            this.caller = caller;
            this.id = id;
            final Variabler v = new Variabler(this.json, caller, "");
            this.inventory = Bukkit.createInventory(null, v.jsonInt("rows") * 9, McStringUtils.colorText(v.jsonString("title")));
            Menu.this.handlers.put(caller, this);
        }

        private void begin() throws Exception {
            Bukkit.getPluginManager().registerEvents(this, Module.lobby);
            this.parse();
            this.open();
        }

        private void done() {
            this.done(true);
        }

        private void done(final boolean b) {
            if (this.done) {
                return;
            }
            this.done = true;
            HandlerList.unregisterAll(this);
            this.actionsLeftClick.clear();
            this.actionsRightClick.clear();
            if (b) {
                Menu.this.handlers.remove(this.caller, this);
            }
        }

        private void parse() throws Exception {
            this.inventory.clear();
            this.actionsLeftClick.clear();
            this.actionsRightClick.clear();
            final ItemVariabler itemVariabler = new ItemVariabler(this.json.getConfigurationSection("content"), this.caller) {
                public void item(final int slot, final ItemStack stack) {
                    Handler.this.inventory.setItem(slot, stack);
                }

                public void actionLeftClick(final int slot, final List<String> actions) {
                    Handler.this.actionsLeftClick.put(slot, actions);
                }

                public void actionRightClick(final int slot, final List<String> actions) {
                    Handler.this.actionsRightClick.put(slot, actions);
                }
            };
            for (final String sectionId : this.json.getConfigurationSection("content").getKeys(false)) {
                final ConfigurationSection item = this.json.getConfigurationSection("content." + sectionId);
                if (Lobby.replace(this.caller, item.getString("disabled", "false")).equals("false")) {
                    final int row = Integer.parseInt(Lobby.replace(this.caller, item.getString("location.row")));
                    final int column = Integer.parseInt(Lobby.replace(this.caller, item.getString("location.column")));
                    final int slot = (row - 1) * 9 + column - 1;
                    itemVariabler.iteration(sectionId, slot);
                }
            }
        }

        private void open() {
            this.caller.openInventory(this.inventory);
        }

        private boolean inventoryCheck(final Inventory inventory) {
            return inventory != null && inventory.equals(this.inventory);
        }

        @EventHandler
        public void onInventoryClick(final InventoryClickEvent e) {
            try {
                final Inventory inv = e.getClickedInventory();
                if (e.getWhoClicked() instanceof Player) {
                    final Player caller = (Player) e.getWhoClicked();
                    if (caller == this.caller && this.inventoryCheck(inv) && e.getCurrentItem() != null) {
                        final int slot = e.getSlot();
                        List<String> actions = null;
                        switch (e.getClick()) {
                            case LEFT: {
                                actions = this.actionsLeftClick.get(slot);
                                break;
                            }
                            case RIGHT: {
                                actions = this.actionsRightClick.get(slot);
                                break;
                            }
                        }
                        if (actions != null) {
                            Lobby.callActions(caller, actions);
                        }
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        @EventHandler
        public void onInventoryClose(final InventoryCloseEvent e) {
            final Player caller = (Player) e.getPlayer();
            if (caller == this.caller && this.inventoryCheck(e.getInventory())) {
                this.done();
            }
        }

        @EventHandler
        public void onQuit(final PlayerQuitEvent e) {
            if (e.getPlayer() == this.caller) {
                this.done();
            }
        }
    }
}
