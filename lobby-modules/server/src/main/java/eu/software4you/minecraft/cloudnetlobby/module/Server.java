package eu.software4you.minecraft.cloudnetlobby.module;

import de.dytanic.cloudnet.driver.service.ServiceInfoSnapshot;
import de.dytanic.cloudnet.wrapper.Wrapper;
import org.bukkit.entity.Player;

public class Server extends Placeholder {
    public Server() {
        super("Server");
    }

    protected String replace(final Player caller, final String arg) {
        ServiceInfoSnapshot service = Wrapper.getInstance().getCurrentServiceInfoSnapshot();
        String cmd = arg;
        String server = service.getServiceId().getName();
        if (arg.contains(":")) {
            final String[] args = arg.split(":");
            cmd = args[0];
            server = arg.substring(cmd.length() + 1);
        }
        service = Wrapper.getInstance().getCloudServiceByName(server);
        final String s = cmd;
        switch (s) {
            case "IsOnline": {
                return (service != null) ? String.valueOf(service.isConnected()) : "false";
            }
            case "MOTD": {
                return (service != null) ? service.getProperties().getString("Motd") : "null";
            }
            case "Players": {
                return (service != null) ? String.valueOf(service.getProperties().getInt("Online-Count")) : "-1";
            }
            case "MaxPlayers": {
                return (service != null) ? String.valueOf(service.getProperties().getInt("Max-Players")) : "-1";
            }
            case "Name": {
                return server;
            }
            default: {
                return this.id + ":" + arg;
            }
        }
    }

    protected void load() {
    }

    protected void unload() {
    }
}
