package eu.software4you.minecraft.cloudnetlobby.module;

import org.bukkit.entity.Player;

public class Inventory extends Action {
    public Inventory() throws Exception {
        super("Inventory");
    }

    protected boolean call(final Player caller, final String arg, final boolean quit) {
        switch (arg) {
            case "Close": {
                caller.closeInventory();
                break;
            }
        }
        return true;
    }

    protected void load() {
    }

    protected void unload() {
    }
}
