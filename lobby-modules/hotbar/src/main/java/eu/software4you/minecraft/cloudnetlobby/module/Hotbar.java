package eu.software4you.minecraft.cloudnetlobby.module;

import eu.software4you.configuration.file.JsonConfiguration;
import eu.software4you.minecraft.McStringUtils;
import eu.software4you.minecraft.cloudnetlobby.Lobby;
import eu.software4you.minecraft.cloudnetlobby.parsing.ItemVariabler;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.io.File;
import java.util.HashMap;
import java.util.List;

public class Hotbar extends Action {
    private final HashMap<String, JsonConfiguration> registeredHotbars;
    private final HashMap<Player, Handler> handlers;

    public Hotbar() {
        super("Hotbar");
        this.registeredHotbars = new HashMap<String, JsonConfiguration>();
        this.handlers = new HashMap<Player, Handler>();
    }

    protected boolean call(final Player caller, final String arg, final boolean quit) throws Exception {
        if (arg.equals("~done") && this.handlers.containsKey(caller)) {
            this.handlers.get(caller).done();
        } else if (arg.equals("~reload") && this.handlers.containsKey(caller)) {
            final String id = this.handlers.get(caller).id;
            this.handlers.get(caller).done();
            this.call(caller, id, quit);
        } else {
            new Handler(caller, arg).begin();
        }
        return true;
    }

    protected void load() {
        new File(this.getDataFolder(), "hotbars").mkdirs();
        this.saveResource("hotbars/default.json", false);
        for (final File hotbar : new File(this.getDataFolder(), "hotbars").listFiles(f -> f.getName().endsWith(".json"))) {
            final JsonConfiguration json = JsonConfiguration.loadConfiguration(hotbar);
            this.registerHotbar(hotbar.getName().substring(0, hotbar.getName().lastIndexOf(".")), json);
        }
    }

    protected void unload() {
        this.registeredHotbars.clear();
        this.handlers.forEach((c, h) -> {
            c.getInventory().clear();
            h.done(false);
            return;
        });
        this.handlers.clear();
    }

    private void validateHotbarRegistered(final String id, final String thr) {
        if (!this.isHotbar(id)) {
            throw new IllegalAccessError(thr);
        }
    }

    private void validateHotbarNotRegistered(final String id, final String thr) {
        if (this.isHotbar(id)) {
            throw new IllegalAccessError(thr);
        }
    }

    public boolean isHotbar(final String id) {
        return this.registeredHotbars.containsKey(id);
    }

    private void registerHotbar(final String id, final JsonConfiguration json) {
        this.validateHotbarNotRegistered(id, "Tried to register an already registered menu ('" + id + "')");
        this.registeredHotbars.put(id, json);
    }

    private void unregisterHotbar(final String id) {
        this.validateHotbarRegistered(id, "Tried to unregister a non-registered menu ('" + id + ')');
        this.registeredHotbars.remove(id);
    }

    private class Handler implements Listener {
        private final JsonConfiguration json;
        private final Player caller;
        private final String id;
        private final HashMap<Integer, List<String>> actionsLeftClick;
        private final HashMap<Integer, List<String>> actionsRightClick;
        private final HashMap<Integer, ItemStack> hotbarItems;
        private boolean done;

        private Handler(final Player caller, final String id) throws Exception {
            this.actionsLeftClick = new HashMap<Integer, List<String>>();
            this.actionsRightClick = new HashMap<Integer, List<String>>();
            this.hotbarItems = new HashMap<Integer, ItemStack>();
            this.done = false;
            if (!Hotbar.this.isHotbar(id)) {
                throw new IllegalAccessError("Tried to load an non-registered hotbar ('" + id + "') for " + caller.getName());
            }
            if (Hotbar.this.handlers.containsKey(caller)) {
                throw new IllegalAccessError("Tried to overload hotbar ('" + Hotbar.this.handlers.get(caller).id + "') with menu ('" + id + "') for " + caller.getName());
            }
            this.json = Hotbar.this.registeredHotbars.get(id);
            this.caller = caller;
            this.id = id;
            Hotbar.this.handlers.put(caller, this);
        }

        private void begin() throws Exception {
            Bukkit.getPluginManager().registerEvents(this, Module.lobby);
            this.parse();
            this.give();
        }

        private void done() {
            this.done(true);
        }

        private void done(final boolean b) {
            if (this.done) {
                return;
            }
            this.done = true;
            HandlerList.unregisterAll(this);
            this.actionsLeftClick.clear();
            this.actionsRightClick.clear();
            this.hotbarItems.clear();
            if (b) {
                Hotbar.this.handlers.remove(this.caller, this);
            }
        }

        private void parse() throws Exception {
            this.hotbarItems.clear();
            this.actionsLeftClick.clear();
            this.actionsRightClick.clear();
            final String title = McStringUtils.colorText(this.json.getString("title"));
            final ItemVariabler iv = new ItemVariabler(this.json.getConfigurationSection("content"), this.caller) {
                public void item(final int slot, final ItemStack stack) {
                    Handler.this.hotbarItems.put(slot, stack);
                }

                public void actionLeftClick(final int slot, final List<String> actions) {
                    Handler.this.actionsLeftClick.put(slot, actions);
                }

                public void actionRightClick(final int slot, final List<String> actions) {
                    Handler.this.actionsRightClick.put(slot, actions);
                }
            };
            for (final String item : this.json.getConfigurationSection("content").getKeys(false)) {
                iv.iteration(item, this.json.getInt("content." + item + ".slot"));
            }
        }

        private void give() {
            final PlayerInventory inv = this.caller.getInventory();
            int slot = 0;
            for (final ItemStack stack : inv.getContents()) {
                if (stack != null && !stack.getType().equals(Material.AIR) && !this.hotbarItems.containsKey(slot)) {
                    inv.removeItem(stack);
                }
                ++slot;
            }
            this.hotbarItems.forEach((i, s) -> {
                if (inv.getItem(i) == null || !inv.getItem(i).isSimilar(s)) {
                    inv.setItem(i, s);
                }
            });
        }

        @EventHandler
        public void onItemUse(final PlayerInteractEvent e) {
            try {
                final Player caller = e.getPlayer();
                if (caller == this.caller && !e.getAction().equals(org.bukkit.event.block.Action.PHYSICAL) && this.hotbarItems.containsKey(caller.getInventory().getHeldItemSlot()) && e.getHand() == EquipmentSlot.HAND) {
                    final int slot = caller.getInventory().getHeldItemSlot();
                    List<String> actions = null;
                    switch (e.getAction()) {
                        case LEFT_CLICK_AIR:
                        case LEFT_CLICK_BLOCK: {
                            actions = this.actionsLeftClick.get(slot);
                            break;
                        }
                        case RIGHT_CLICK_AIR:
                        case RIGHT_CLICK_BLOCK: {
                            actions = this.actionsRightClick.get(slot);
                            break;
                        }
                    }
                    if (actions != null) {
                        Lobby.callActions(caller, actions);
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        @EventHandler
        public void onQuit(final PlayerQuitEvent e) {
            if (e.getPlayer() == this.caller) {
                this.done();
            }
        }
    }
}
