package eu.software4you.minecraft.cloudnetlobby.module;

import de.dytanic.cloudnet.driver.service.ServiceInfoSnapshot;
import de.dytanic.cloudnet.driver.service.ServiceTask;
import de.dytanic.cloudnet.wrapper.Wrapper;
import eu.software4you.minecraft.cloudnetlobby.configuration.Layout;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.Messenger;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Connect extends Action {
    public Connect() throws Exception {
        super("Connect");
        final Messenger messenger = Connect.lobby.getServer().getMessenger();
        if (!messenger.isOutgoingChannelRegistered(Connect.lobby, "BungeeCord")) {
            messenger.registerOutgoingPluginChannel(Connect.lobby, "BungeeCord");
        }
    }

    protected boolean call(final Player caller, final String arg, final boolean quit) {
        final ServiceInfoSnapshot serviceInfoSnapshot = Wrapper.getInstance().getCloudServiceByName(arg);
        if (serviceInfoSnapshot == null) {
            return false;
        }
        if (!quit) {
            final ServiceTask task = Wrapper.getInstance().getServiceTask(serviceInfoSnapshot.getServiceId().getTaskName());
            if (task.isStaticServices()) {
                Layout.connectingServer.send(caller, arg);
            } else {
                Layout.connectingLobby.send(caller, arg);
            }
        }
        try (final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
             final DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream)) {
            dataOutputStream.writeUTF("Connect");
            dataOutputStream.writeUTF(serviceInfoSnapshot.getServiceId().getName());
            caller.sendPluginMessage(Connect.lobby, "BungeeCord", byteArrayOutputStream.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    protected void load() {
    }

    protected void unload() {
    }
}
