package eu.software4you.minecraft.cloudnetlobby.module;

import eu.software4you.minecraft.cloudnetlobby.Lobby;
import org.bukkit.entity.Player;

import java.util.regex.Pattern;

public class PlayerAction extends Action {
    public PlayerAction() throws Exception {
        super("PlayerAction");
    }

    protected boolean call(final Player caller, final String arg, final boolean quit) {
        if (arg == null || arg.isEmpty()) {
            return false;
        }
        final String[] args = arg.split(":");
        final String lowerCase = args[0].toLowerCase();
        switch (lowerCase) {
            case "message": {
                if (arg.contains(":")) {
                    caller.sendMessage(Lobby.replaceC(caller, arg.substring(args[0].length() + 1).replace("{newline}", "\n")));
                    break;
                }
                break;
            }
            case "chat": {
                if (arg.contains(":")) {
                    caller.chat(Lobby.replaceC(caller, arg.substring(args[0].length() + 1)));
                    break;
                }
                break;
            }
            case "title": {
                if (args.length >= 5) {
                    final int fadeIn = Integer.parseInt(args[1]);
                    final int stay = Integer.parseInt(args[2]);
                    final int fadeOut = Integer.parseInt(args[3]);
                    final String title = arg.substring(args[0].length() + 1 + args[1].length() + 1 + args[2].length() + 1 + args[3].length() + 1);
                    final String[] lines = Lobby.replaceC(caller, title).split(Pattern.quote("{newline}"));
                    caller.sendTitle(lines[0], title.contains("{newline}") ? lines[1] : "", fadeIn, stay, fadeOut);
                    break;
                }
                break;
            }
        }
        return true;
    }

    protected void load() {
    }

    protected void unload() {
    }
}
