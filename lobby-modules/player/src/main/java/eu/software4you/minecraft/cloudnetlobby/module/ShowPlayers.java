package eu.software4you.minecraft.cloudnetlobby.module;

import eu.software4you.configuration.ConfigurationSection;
import eu.software4you.configuration.file.YamlConfiguration;
import eu.software4you.minecraft.cloudnetlobby.Lobby;
import eu.software4you.minecraft.cloudnetlobby.configuration.Layout;
import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class ShowPlayers extends Action implements Listener {
    public ShowPlayers() {
        super("ShowPlayers");
    }

    public static Showing get(final Player p) {
        return has(p) ? Showing.valueOf(String.valueOf(p.getMetadata("playersShowing").get(0).value())) : Showing.All;
    }

    public static boolean has(final Player p) {
        return p.hasMetadata("playersShowing") && p.getMetadata("playersShowing").size() > 0;
    }

    public static void set(final Player p, final Showing showing) {
        setMetadata(p, "playersShowing", showing.name());
    }

    public static void removeMedatadata(final Entity ent, final String name) {
        if (ent.hasMetadata(name)) {
            ent.removeMetadata(name, ShowPlayers.lobby);
        }
    }

    public static void setMetadata(final Entity ent, final String name, final Object value) {
        removeMedatadata(ent, name);
        ent.setMetadata(name, new FixedMetadataValue(ShowPlayers.lobby, value));
    }

    protected boolean call(final Player caller, final String arg, final boolean quit) throws Exception {
        Showing showing = arg.equals("") ? (has(caller) ? get(caller) : Showing.All) : Showing.valueOf(arg);
        final YamlConfiguration y = PlayerHider.inst.yaml;
        y.set("prefix", Lobby.replaceC(caller, y.getString("prefix")).replace("%plugin_prefix%", Layout.prefix.get()));
        final ConfigurationSection sec = y.getConfigurationSection("showing");
        switch (showing) {
            case None: {
                Bukkit.getOnlinePlayers().forEach(caller::hidePlayer);
                if (!quit) {
                    caller.sendMessage(Lobby.replaceC(caller, sec.getString("none")));
                    break;
                }
                break;
            }
            case OnlyFriends: {
                if (!quit) {
                    caller.sendMessage(Lobby.replaceC(caller, sec.getString("friends")));
                }
                ShowPlayers.lobby.async(() -> {
                    Bukkit.getOnlinePlayers().forEach(pp -> {
                        if (!Placeholder.replace(caller, "IsFriend", pp.getUniqueId().toString()).equals("true")) {
                            ShowPlayers.lobby.sync(() -> caller.hidePlayer(ShowPlayers.lobby, pp));
                        } else {
                            ShowPlayers.lobby.sync(() -> caller.showPlayer(ShowPlayers.lobby, pp));
                        }
                    });
                    return;
                });
                break;
            }
            default: {
                showing = Showing.All;
                Bukkit.getOnlinePlayers().forEach(caller::showPlayer);
                if (!quit) {
                    caller.sendMessage(Lobby.replaceC(caller, sec.getString("all")));
                    break;
                }
                break;
            }
        }
        set(caller, showing);
        return true;
    }

    protected void load() {
        Bukkit.getPluginManager().registerEvents(this, ShowPlayers.lobby);
    }

    protected void unload() {
        HandlerList.unregisterAll(this);
    }

    @EventHandler
    public void onJoin(final PlayerJoinEvent e) {
        try {
            for (final Player pp : Bukkit.getOnlinePlayers()) {
                if (e.getPlayer() != pp) {
                    this.call(pp, "", true);
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public enum Showing {
        All,
        OnlyFriends,
        None;

        public static String[] names() {
            final String[] array = ArrayUtils.EMPTY_STRING_ARRAY;
            for (final Showing value : values()) {
                ArrayUtils.add(array, value.name());
            }
            return array;
        }

        @Override
        public String toString() {
            return this.name();
        }
    }
}
