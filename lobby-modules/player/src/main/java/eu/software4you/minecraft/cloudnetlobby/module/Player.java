package eu.software4you.minecraft.cloudnetlobby.module;

import eu.software4you.minecraft.spigot.psb.PSB;

public class Player extends Placeholder {
    public Player() {
        super("Player");
    }

    protected String replace(final org.bukkit.entity.Player caller, String arg) {
        final String cmd = arg.contains(":") ? arg.substring(0, arg.indexOf(":")) : arg;
        arg = (arg.contains(":") ? arg.substring(cmd.length() + 1) : "");
        switch (cmd) {
            case "Name": {
                return caller.getName();
            }
            case "DisplayName": {
                return caller.getDisplayName();
            }
            case "UUID": {
                return caller.getUniqueId().toString();
            }
            case "HasPerm": {
                return String.valueOf(caller.hasPermission(arg));
            }
            case "HeldSlot": {
                return String.valueOf(caller.getInventory().getHeldItemSlot());
            }
            case "IsFriend": {
                String answer = null;
                try {
                    answer = PSB.request("Friends IsFriend " + caller.getUniqueId() + " " + arg);
                } catch (Exception ignored) {
                }
                return (answer != null) ? answer : "false";
            }
            default: {
                return null;
            }
        }
    }

    protected void load() throws Exception {
        registerModule(new PlayerAction());
    }

    protected void unload() {
    }
}
