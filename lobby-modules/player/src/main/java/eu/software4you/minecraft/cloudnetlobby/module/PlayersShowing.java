package eu.software4you.minecraft.cloudnetlobby.module;

import org.bukkit.entity.Player;

public class PlayersShowing extends Placeholder {
    public PlayersShowing() {
        super("PlayersShowing");
    }

    protected String replace(final Player caller, final String arg) {
        if (caller == null) {
            return "";
        }
        final ShowPlayers.Showing showing = ShowPlayers.get(caller);
        if (showing != null) {
            return showing.name();
        }
        return ShowPlayers.Showing.All.name();
    }

    protected void load() {
    }

    protected void unload() {
    }
}
