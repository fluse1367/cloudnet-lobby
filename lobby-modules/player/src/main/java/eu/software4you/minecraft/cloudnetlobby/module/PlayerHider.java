package eu.software4you.minecraft.cloudnetlobby.module;

import eu.software4you.configuration.file.YamlConfiguration;

import java.io.File;

public class PlayerHider extends Module {
    static PlayerHider inst;
    YamlConfiguration yaml;

    public PlayerHider() {
        super("PlayerHider");
    }

    protected void load() throws Exception {
        PlayerHider.inst = this;
        final File layoutFile = new File(this.getDataFolder(), "layout_playerhider.yml");
        this.saveResource("layout_playerhider.yml", false);
        this.yaml = YamlConfiguration.loadConfiguration(layoutFile);
        registerModule(new PlayersShowing());
        registerModule(new ShowPlayers());
    }

    protected void unload() {
    }
}
